require_relative "./src/file_reader/log_parser.rb"

unless ARGV.empty?
    file_path = ARGV[0] #require parameter on line of comand
else
    raise "Parameter is not a valid file path"
end

opened_file = LogParser.new(file_path)
first_line = opened_file.print_first_line


puts first_line
puts opened_file.jsonfy_obj  
opened_file.close_file
