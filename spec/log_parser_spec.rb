require_relative '../src/file_reader/log_parser.rb' 
require 'json'

describe 'LogParser' do

	let(:file_path) { "./spec/fixtures/TESTE2.log" }
  
		it 'rejects unmatch paths for files' do

			file = LogParser.new(file_path) #relative path to some file
			expect(file).to be_truthy
		end		
end

describe 'jsonfy_obj' do
	let (:file_path) { "./spec/fixtures/TESTE2.log" }
	it 'Recieves file name and number of lines, creates a Ruby object and converts it to a json object' do

	opened_file = LogParser.new(file_path)

	json_obj = opened_file.jsonfy_obj
	expect(json_obj).to eq("{\"TESTE2.log\":{\"lines\":2}}")
	end
end
