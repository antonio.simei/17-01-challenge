require 'json'

class LogParser
    def initialize(file_path)
    if File.file?(file_path)
        @file = File.open(file_path)
    else
        raise "Path not found"
    end
    end

    
    def jsonfy_obj
      file_stats = { 
        name() =>{
          lines: counter()
        }
      }
      file_stats.to_json    
    end
    
    def print_first_line
        line = @file.readlines[0]
        @file.seek(0)
        return line
    end
    
    def close_file
        file_closed = @file.close
    end

    
    private 

    def name()
      File.basename(@file.path)
    end

    def counter
      @file.each.inject(0){|c, line| c +1}
    end
end
