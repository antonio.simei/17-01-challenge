## Installation 
In order to run this project, you should have Ruby 3.1.0 installed
First, you must update GPG to the most recent version.

```bash
sudo apt install gnupg2
```
The next steps doesn't require sudo privileges, so they can be ran as normal user.

Next, we request the RVM project's key to sign each RVM release.
```bash
gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
```

In order to download the RVM script, we should move to a writable, in this case, the `/tmp` directory and then download the rvm scrip into a file

```bash
cd /tmp
curl -sSL https://get.rvm.io -o rvm.sh
```
Now, we can install Ruby's specific version using the following command
For this project, recommendation is to install Ruby 3.1.0 

``` bash
rvm install ruby_version
 ```

 To check installation process, you should use `rvm which ruby` in your terminal
 It should return the following path `home/username`
 
 For this project, you should also install bundler for management:
 ```bash
 gem install bundler
 ```
 Next, let's setup the Rspec gem for testing our code:

 ```bash
 bundle init 
 ```
 At the Gemfile created, add `gem 'rspec'`. This declaration can also receive another argument for the gem version that you want to install.

